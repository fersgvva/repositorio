﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CandyController : MonoBehaviour {

    private int value;

    private void Awake()
    {
        if(gameObject.name.Contains("Green"))
        {
            value = 1;
        }
        else if(gameObject.name.Contains("Blue"))
        {
            value = 5;
        }
        else if (gameObject.name.Contains("Red"))
        {
            value = 20;
        }
        else if (gameObject.name.Contains("Purple"))
        {
            value = 50;
        }
        else if (gameObject.name.Contains("Teal"))
        {
            value = 100;
        }
        else if (gameObject.name.Contains("Gold"))
        {
            value = 300;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Player.p1.lvlCandies += value;
            LevelManager.lvl.candyCount.text = Player.p1.lvlCandies.ToString();
            LevelManager.lvl.candyCountAnim.Play("CandyCountAnim", -1, 0f);
            Destroy(gameObject);
        }
    }
}
