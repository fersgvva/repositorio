﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitController : MonoBehaviour {

    public delegate void Action();
    public static event Action EnteringLimit;
    public float xMin, xMax;
    public Transform[] limits;

    CameraController cam;
    private float height, width;

    private void Awake()
    {
        cam = Camera.main.GetComponent<CameraController>();
        limits = GetComponentsInChildren<Transform>();
    }

    private void Start()
    {
        height = 2f * Camera.main.orthographicSize;
        width = height * Camera.main.aspect;
    }

    private void Update()
    {
        if (Camera.main.transform.position.x - width/2 >= limits[1].position.x)
        {
            cam.xMin = limits[1].position.x + width / 2; //Limita los puntos minimos y máximos del pivote de la cámara.
            cam.xMax = limits[2].position.x - width / 2;
            Player.p1.xMin = limits[1].position.x;
            Player.p1.xMax = limits[2].position.x;
            LevelManager.lvl.xMin = limits[1].position.x;
            LevelManager.lvl.xMax = limits[2].position.x;
            EnteringLimit();
            NewLimit();
        }
    }

    private void NewLimit()
    {
        transform.position = transform.position + new Vector3(54f, 0f, 0f);
    }
}
