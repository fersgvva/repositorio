﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scrolling : MonoBehaviour {

    Vector3 initPos;
    float bgSize;
    public float scrollSpeed;
	// Use this for initialization
	void Start ()
    {
        initPos = transform.position;
        bgSize = 58;
	}
	
	// Update is called once per frame
	void Update ()
    {
        float newPos = Mathf.Repeat(Time.time * scrollSpeed, bgSize);
        transform.position = initPos + Vector3.left * newPos;
		
	}
}
