﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour {

    public float speed;
    float lastCamPos;
	// Use this for initialization
	void Start () {
        lastCamPos = Camera.main.transform.position.x;
	}
	
	// Update is called once per frame
	void Update ()
    {
        float delta = Camera.main.transform.position.x - lastCamPos;
        transform.position += Vector3.left * (delta * speed);
        lastCamPos = Camera.main.transform.position.x;
		
	}
}
