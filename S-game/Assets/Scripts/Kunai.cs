﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kunai : MonoBehaviour {

    Rigidbody2D rb;
    float force;
    //float step;

    void Awake()
    {   
        force = 10;
        rb = GetComponent<Rigidbody2D>();
        //step = Time.deltaTime * force;
    }
    // Use this for initialization
    void Start () {
        if (Player.p1.transform.rotation == Quaternion.identity)
            rb.AddForce(Vector2.right * force, ForceMode2D.Impulse);
        else
            rb.AddForce(Vector2.left * force, ForceMode2D.Impulse);
    }
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(0, 0, 15);
	}

    void FixedUpdate()
    {
        //if (Player.p1.transform.rotation == Quaternion.identity)
        //    rb.position = Vector2.MoveTowards(rb.position, Vector2.right, step);
        //else
        //    rb.position = Vector2.MoveTowards(rb.position, Vector2.left, step);
    }



}
