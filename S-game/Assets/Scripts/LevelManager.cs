﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {


    public GameObject[] candies;
    public static LevelManager lvl;
    public GameObject enemy;
    public LimitController limit;
    public List<GameObject> activePlayers;
    public Text candyCount;
    [HideInInspector] public Animator candyCountAnim;
    [HideInInspector] public bool onWave;
    [HideInInspector] public int enemiesCurrWave;
    [HideInInspector] public float xMin, xMax;
    private int numWaves;
    private void Awake()
    {
        if (lvl == null)
            lvl = this;
        else
        {
            if (lvl != this)
            {
                Destroy(gameObject);
            }
        }
    }
    // Use this for initialization
    void Start () {
        candyCountAnim = candyCount.GetComponent<Animator>();
        onWave = false;
	}

    private void OnEnable()
    {
        LimitController.EnteringLimit += StartWave;
    }
    private void OnDisable()
    {
        LimitController.EnteringLimit -= StartWave;
    }
    // Update is called once per frame
    void Update ()
    {
        if (onWave && enemiesCurrWave == 0) //Mueren todos los enemigos de la oleada.
        {
            onWave = false;
            if(numWaves == 2)
            {
                GameManager.gM.SaveLevelData();
                GameManager.gM.LoadMenu();
            }
        }
	}

    private void StartWave()
    {
        StartCoroutine(Wave());
    }
    private IEnumerator Wave()
    {
        onWave = true;
        numWaves++;
        int numEnemies = CalculateEnemies();
        for (int i = 0; i < numEnemies; i++)
        {
            float spawnPointX = RandomSpawnPoint();
            Instantiate(enemy,
                new Vector3(spawnPointX, -2.77f, 0),
                    Quaternion.identity);
            yield return new WaitForSeconds(RandomTimeBetweenEnemies());
        }

    }

    private int CalculateEnemies()
    {
        return enemiesCurrWave = Random.Range(8, 13);
    }
    private float RandomSpawnPoint()
    {
        float spawnPoint;
        int side = Random.Range(0, 2); //Izq o dcha.
        spawnPoint = side == 0 ? xMin : xMax;
        return spawnPoint;
    }

    private float RandomTimeBetweenEnemies()
    {
        return Random.Range(0.2f, 3f);
    }


}
