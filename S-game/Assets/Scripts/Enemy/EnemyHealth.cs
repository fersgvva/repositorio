﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour {


    SpriteRenderer sprite;
    private Animator anim;
    private EnemyMovement movement;
    private EnemyCanvas canvas;
    private float life;
    private GameObject[] holdedCandies;
    // Use this for initialization
    private void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        movement = GetComponent<EnemyMovement>();
        canvas = GetComponentInChildren<EnemyCanvas>();
        life = 50;
    }

    private void Start()
    {
        holdedCandies = new GameObject[Random.Range(3, 7)];

        for (int i = 0; i < holdedCandies.Length; i++) //Entre 3 y 6 caramelos por enemigo.
        {
            float prob = Random.value;
            if (prob <= 0.02f) // 2% dorado
            {
                holdedCandies[i] = LevelManager.lvl.candies[5];
            }
            else if (prob > 0.02f && prob <= 0.10f) // 8% plateado
            {
                holdedCandies[i] = LevelManager.lvl.candies[4];
            }
            else if (prob > 0.10f && prob <= 0.2f) //10% morada
            {
                holdedCandies[i] = LevelManager.lvl.candies[3];
            }
            else if (prob > 0.2f && prob <= 0.35f) //15% roja
            {
                holdedCandies[i] = LevelManager.lvl.candies[2];
            }
            else if (prob > 0.35f && prob <= 0.6f) //25% azul
            {
                holdedCandies[i] = LevelManager.lvl.candies[1];
            }
            else if (prob > 0.6f && prob <= 1) //40% verde
            {
                holdedCandies[i] = LevelManager.lvl.candies[0];
            }
        }

       
    }

    public void TakeDamage(int baseAttack)
    {
        float dmg = Random.Range(baseAttack - baseAttack * 0.1f, baseAttack + baseAttack * 0.1f);
        dmg = Mathf.Round(dmg);
        life -= dmg;
        canvas.ShowDamageText(dmg);
        StartCoroutine(HitEffect());
        if(life <=0)
        {
            OnDeath();
        }
    }


    IEnumerator HitEffect()
    {
        sprite.material.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        sprite.material.color = Color.white;
    }

    private void OnDeath()
    {
        LevelManager.lvl.enemiesCurrWave--;
        gameObject.tag = "Death";
        movement.rb.isKinematic = false;
        gameObject.layer = LayerMask.NameToLayer("Death");
        movement.enabled = false;
        anim.SetTrigger("die");
        ExplodeCandies();
        StartCoroutine(OnDeathEffect());
        Destroy(gameObject, 5);
    }

    IEnumerator OnDeathEffect()
    {
        float flickeringTime=0.5f;
        yield return new WaitForSeconds(2f);
        while(true)
        {
            sprite.enabled = sprite.enabled ? false : true;
            yield return new WaitForSeconds(flickeringTime);
            flickeringTime -= 0.05f;
        }
    }

    void ExplodeCandies()
    {
        foreach (GameObject candyPrefab in holdedCandies)
        {
            Quaternion randomRotation = Quaternion.Euler(0, 0, Random.Range(-60f, 60f));
            float randomForce = Random.Range(5f, 8f);
            GameObject candy = Instantiate(candyPrefab, transform.position, randomRotation);
            Rigidbody2D candyRb = candy.GetComponent<Rigidbody2D>();
            candyRb.AddForce(candy.transform.up * randomForce, ForceMode2D.Impulse);
            candyRb.AddTorque(0.5f, ForceMode2D.Impulse);
        }
    }
}
