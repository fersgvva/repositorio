﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    public Rigidbody2D rb;

    private float speed;
    private float step;
    private float offset;
    private Animator anim;

    void Awake()
    {
        speed = 2;
        offset = 2;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        step = Time.deltaTime * speed;
    }
    // Use this for initialization
    void Start ()
    {
        if (LevelManager.lvl.activePlayers.Count == 0)
        {
            OnPlayerDeath();
        }
        else
        {
            PlayerHealth.NotifyDeath += OnPlayerDeath;
        }

    }

    private void Update()
    {
        if (transform.position.x - Player.p1.transform.position.x > 0)
            transform.rotation = Quaternion.Euler(0, 180, 0);
        else
            transform.rotation = Quaternion.Euler(0, 0, 0);

        if (Mathf.Abs(transform.position.x - Player.p1.transform.position.x) <= offset)
        {
            anim.SetBool("attacking", true);
        }
    }
    void FixedUpdate ()
    {
        if (!anim.GetBool("attacking"))
            rb.position = Vector2.MoveTowards(rb.position, Player.p1.transform.position, step);
    }


    public void FinishAnim(string animName)
    {
        anim.SetBool(animName, false);
    }

    void OnPlayerDeath()
    {
        anim.SetTrigger("noPlayers");
        this.enabled = false;
    }

    private void OnDisable()
    {
        PlayerHealth.NotifyDeath -= OnPlayerDeath;
    }

}
