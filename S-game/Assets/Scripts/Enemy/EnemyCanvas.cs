﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyCanvas : MonoBehaviour {

    public Text damageTextPrefab;
    private Text damageText;
    private EnemyMovement enemy;
    // Use this for initialization
    void Start () {
        enemy = GetComponentInParent<EnemyMovement>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowDamageText(float dmg)
    {
        damageText = Instantiate(
            damageTextPrefab,
                transform.position,
                    enemy.transform.rotation * Quaternion.Euler(0,180,0)) as Text;
        damageText.transform.SetParent(transform, false);
        damageText.text = "-" + dmg.ToString();
    }

}
