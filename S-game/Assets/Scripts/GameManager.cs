﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public Text candiesTxt;
    private int allCandies;
    public static GameManager gM;
    private void Awake()
    {
        if (gM == null)
            gM = this;
        else
        {
            if (gM != this)
                Destroy(gM);
        }
        DontDestroyOnLoad(gM);

        allCandies = 0;
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SaveLevelData()
    {
        allCandies += Player.p1.lvlCandies;
    }
    public void LoadMenu()
    {
        SceneManager.LoadScene("Menu");
        candiesTxt.text = allCandies.ToString();
    }
}
