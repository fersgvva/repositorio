﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    Vector3 offset;
    [HideInInspector] public float xMin, xMax;
    [HideInInspector] public float height;

    private float speed = 3;

	// Use this for initialization
	void Start () {
        offset = new Vector3(0, 3.3f, -10);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(Player.p1)    
            transform.position = Vector3.Lerp(transform.position,
                Player.p1.transform.position + offset, speed * Time.deltaTime);
        if(LevelManager.lvl.onWave)
        {
            transform.position = new Vector3(
                Mathf.Clamp(transform.position.x, xMin, xMax),
                transform.position.y,
                -10);
        }
    }

}
