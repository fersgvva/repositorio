﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public static Player p1;

    public Transform spawnPoint;

    public struct PlayerStats
    {
        public int level;
        public int hp;
        public int candies;
        public int baseAttack;
        public float speed;
        public int experience;
        public string name;
    } public PlayerStats stats;

    [HideInInspector] public int lvlCandies;
    public int kunais;
    public Text kunaisCount;
    private Animator anim;
    private Rigidbody2D rb;
    private BoxCollider2D coll;
    private enum State {idle, run, attack, slide, jump, throwKunai, onHit, dead };
    private State prevState,state;
    private float h, v;
    [HideInInspector] public float xMin, xMax;

    void Awake()
    {
        if (p1 == null)
            p1 = this;
        else
        {
            if (p1 != this)
            {
                Destroy(gameObject);
            }
        }
        //DontDestroyOnLoad(gameObject);

        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        coll = GetComponent<BoxCollider2D>();
    }
    // Use this for initialization
    void Start ()
    {
        stats.candies = 0;
        stats.baseAttack = 30;
        stats.speed = 10;
        state = State.idle;
        prevState = State.idle;
        kunais = 3;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(state == State.onHit)
        {
            OnHit();
        }
        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");
        if(state == State.idle)
        {
            Idle();
        }
        else if(state == State.run)
        {
            Run();
        }
        else if(state == State.attack)
        {
            Attack();
        }
        else if(state == State.slide)
        {
            Slide();
        }
        else if(state == State.jump)
        {
            Jump();
        }
        else if(state == State.throwKunai)
        {
            PrepareThrow();
        }

    }

    void Transitions()
    {
        prevState = state;
        if (Input.GetKeyDown(KeyCode.Space))
        {
            state = State.attack;
        }
        else if (Input.GetKeyDown(KeyCode.LeftControl) && anim.GetBool("running"))
        {
            state = State.slide;
        }
        else if (Input.GetMouseButtonDown(0) && kunais > 0)
        {
            state = State.throwKunai;
        }
    }

    void Idle()
    {
        anim.SetBool("running", false);
        if(h!=0)
        {
            state = State.run;
        }
        Transitions();
    }

    void Run()
    {
        if (h > 0)
        {
            anim.SetBool("running", true);
            transform.rotation = Quaternion.Euler(0, 0, 0);
            transform.Translate(new Vector2(h, 0) * stats.speed * Time.deltaTime);
            Transitions();

        }
        else if (h < 0)
        {
            anim.SetBool("running", true);
            transform.rotation = Quaternion.Euler(0, 180, 0);
            transform.Translate(new Vector2(-h, 0) * stats.speed * Time.deltaTime);
            Transitions();
        }
        else if (h == 0)
        {
            state = State.idle;
        }

        if (LevelManager.lvl.onWave)
        {
            transform.position = new Vector3(
                Mathf.Clamp(transform.position.x,
                    xMin + coll.size.x/2, xMax - coll.size.x/2),
                transform.position.y,
                0);
        }

    }

    void Attack()
    {
        anim.SetBool("attacking", true);
    }

    void Jump()
    {
        anim.SetBool("jumping", true);
        rb.AddForce(new Vector2(0, v), ForceMode2D.Impulse);
        //transform.Translate(new Vector2(0, v) * speed * Time.deltaTime);
        if(rb.velocity.y <= 0)
        {
            anim.speed = 0;
            anim.Play("Jump", 0, 0.8f);
        }
    }

    void Slide()
    {
        anim.SetBool("sliding", true);
        stats.speed = 15.0f;
        if (h > 0)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            transform.Translate(new Vector2(h, 0) * stats.speed * Time.deltaTime);
        }

        if (h < 0)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
            transform.Translate(new Vector2(-h, 0) * stats.speed * Time.deltaTime);
        }
        
    }

    void PrepareThrow()
    {
        anim.SetBool("throwing", true);
    }

    public void ThrowKunai(GameObject kunai)
    {
        Instantiate(kunai, spawnPoint.position, Quaternion.Euler(0,0,180));
        kunais--;
        kunaisCount.text = "x" + kunais;
    }




    void OnHit()
    {
        anim.SetBool("beingHit", true);
    }

    public void FinishAnim(string animName)
    {
        anim.SetBool(animName, false);
        coll.enabled = true;
        state = prevState;
    }


}
