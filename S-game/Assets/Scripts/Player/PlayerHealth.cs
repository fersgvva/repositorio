﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    SpriteRenderer sprite;
    private Animator anim;
    private Player movement;
    float life;
    public delegate void Action();
    public static event Action NotifyDeath;
    public Slider healthbar;

    private void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        movement = GetComponent<Player>();
        life = 500;
        healthbar.maxValue = life;
        healthbar.value = life;
    }


    public void TakeDamage()
    {
        life -= 10;
        healthbar.value -= 10;
        StartCoroutine(HitEffect());
        if (life <= 0)
        {
            OnDeath();
        }
    }

    IEnumerator HitEffect()
    {
        sprite.material.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        sprite.material.color = Color.white;
    }

    private void OnDeath()
    {
        NotifyDeath();
        gameObject.layer = LayerMask.NameToLayer("Death");
        gameObject.tag = "Death";
        movement.enabled = false;
        anim.SetTrigger("die");
        LevelManager.lvl.activePlayers.Remove(GameObject.Find("Player"));
        Destroy(gameObject, 5);
    }
}
